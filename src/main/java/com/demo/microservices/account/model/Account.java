package com.demo.microservices.account.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Account {
	private String accNo;
	private String userId;
	private Long productId;
	private Long amount;
	private BigDecimal rate;
}

